 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bayesnetwork;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import unbbayes.io.NetIO;
import unbbayes.prs.Graph;
import unbbayes.prs.Node;
import unbbayes.prs.bn.JunctionTreeAlgorithm;
import unbbayes.prs.bn.ProbabilisticNetwork;
import unbbayes.prs.bn.ProbabilisticNode;
import unbbayes.controller.MainController;
import unbbayes.prs.bn.PotentialTable;




/**
 *
 * @author franciscoh
 */
public class BayesNetwork {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, Exception 
    {   
        if(false)
        {
            entrenamiento(7);
        }
        else
        {
            

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                          

            /**
             * **** Ahora empieza el Algoritmo ****
             */
            
            int diaActual, horario, sm, sv, sp, alarma;
            boolean seguir=true;
            while(seguir)
            {
                 //se carga el Excel
                //Workbook libroExcel = Workbook.getWorkbook(new File("imports/lecturas.xls"));
                //Se Carga el archivo con la estructura  
                File file = new File("imports/bayes2.net");       
                NetIO nIO = new NetIO();
                ProbabilisticNetwork net = (ProbabilisticNetwork) nIO.load(file);

                //Se aplica el algoritmo JunctionTree
                JunctionTreeAlgorithm alg = new JunctionTreeAlgorithm();
                alg.setNetwork(net);
                alg.run();
                
                /**
                 *  Primero obtengo los datos nuevos cada 10 segundos
                 *    - dia:  lo obtengo de este dia con  Date
                 *    - horario: Clasifico el horario si esta dentro de un rango definido
                 *    - sm: Lo obtengo por observación del sensor de movimiento(como?)
                 *    - sv: Lo obtengo por observación del sensor de ventana(como?)
                 *    - sp: Lo obtengo por observación del sensor de puerta(como?)
                 */
                    
                  
                // Lectura de los datos
                System.out.print ("Introduce dia (1..7): ");
                String cadena = br.readLine();
                diaActual = Integer.parseInt(cadena);
                
                System.out.print ("Introduce hora (0..23): ");
                cadena = br.readLine();
                int aux = Integer.parseInt(cadena);
                horario = getHorario(aux);
                
                System.out.print ("Sensor de movimiento(1-0): ");
                cadena = br.readLine();
                sm = Integer.parseInt(cadena);
                
                System.out.print ("Sensor de ventana(1-0): ");
                cadena = br.readLine();
                sv = Integer.parseInt(cadena);
                
                System.out.print ("Sensor de puerta(1-0): ");
                cadena = br.readLine();
                sp = Integer.parseInt(cadena);
                
                   
              
                 

                 /**
                 *    Luego propago la funciones de probabilidad 
                 *    (se inserta la evidencia( finding ) para los nodos dia, horario, sm, sv, sp)
                 *    y veo si activo la alarma o no
                 */

                 // propaga las evidencias
                 ProbabilisticNode fdia,fhorario, fsm, fsv, fsp;
                     fsm   = (ProbabilisticNode)net.getNode(Nodo.SM.name());
                     fsv   = (ProbabilisticNode)net.getNode(Nodo.SV.name());
                     fsp   = (ProbabilisticNode)net.getNode(Nodo.SP.name());
                     fdia   = (ProbabilisticNode)net.getNode(Nodo.Dia.name());
                     fhorario  = (ProbabilisticNode)net.getNode(Nodo.horarios.name());

                     if(sm==0) fsm.addFinding(1); // 100% (inactivo)
                     else if(sm==1) fsm.addFinding(0); // 100% (activo)

                     if(sv==0) fsv.addFinding(1); // 100% (inactivo)
                     else if(sv==1) fsv.addFinding(0); // 100% (activo)

                     if(sp==0) fsp.addFinding(1); // 100% (inactivo)
                     else if(sp==1) fsp.addFinding(0); // 100% (activo)

                     fdia.addFinding(diaActual-1);
                     fhorario.addFinding(horario-1);

                 net.updateEvidences();// Actualiza las evidencias


                 //obtener e imprimir las probabilidades del nodo alarma
                 ProbabilisticNode node2 = (ProbabilisticNode) net.getNode("alarma");
                 System.out.println("");
                 System.out.println("---------------Informe Resultado---------------");
                 System.out.println("Para el dia: "+getDia(diaActual));
                 System.out.println("En el horario: "+horario);
                 System.out.println("Con Sensor Movimiento "+activacion(sm));
                 System.out.println("Con Sensor Ventana "+activacion(sv));
                 System.out.println("Con Sensor Puerta "+activacion(sp));
                 System.out.println("Probabilidades de activar la alarma:");
                 for (int i = 0; i < node2.getStatesSize(); i++) 
                 {        
                     System.out.println(""+ node2.getStateAt(i) + " : " + ((ProbabilisticNode)node2).getMarginalAt(i));
                 }
                 System.out.println("Por lo que, activar la alarma es "+activarAlarma(node2.getMarginalAt(0), (float) 0.5));
                 System.out.println("1 es activado, 0 no activado :)");
                 
                 System.out.println("---------------Fin Informe Resultado---------------");
                 System.out.println("");
                 
                 System.out.print ("fue falsa alarma?(y/n): ");
                 if("y".equals(br.readLine()))
                 {
                    
                    alarma = 0;
                 }
                 else
                 {
                     alarma = 1;
                 }
                 
                 /**
                  * Por Ultimo el aprendizaje de la red, 
                  * - Se actualiza las tablas de probabilidades con las nuevas 
                  *   frecuencias(archivo.net)
                  * - y se agregan las evidencias a la base de datos
                  */
                 //conecto a la bd y agrego un nuevo registro

                 // para cada variable de probabilidad(sm,sv,sp) la frecuencia

                 //actualizo las tablas con el codigo que se encuentra en README
                 actualizarConocimiento(net, nIO, file, diaActual, horario, sm, sv, sp,alarma);
                
                 
                System.out.print ("seguir? (y/n): ");
                System.out.println("");
                 if("n".equals(br.readLine()))
                 {
                     seguir = false;
                 }
                

            }
        }
        
    }

    
    public enum Nodo {
         SM,SV, SP, T, Dia, horarios, alarma
    }
    /**
     * 
     * @param sensor(0,1) 
     * @return activado o no activado
     */
    static public String activacion(int sensor)
    {
        if(sensor==1) return "Activado";
        
        return "No Activado";
    }
    /**
     * define si se activa la alarma o no, dependiendo de las probabilidades
     * de las variables.
     * @param pActivado
     * @param umbral
     * @return alarma activada, alarma no activada
     */
    static public int activarAlarma(float pActivado, float umbral)
    {
                
                
        if(pActivado>umbral)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    
    
    
    /**
     * Segun la hora del dia, nos dice en que horario está
     * @param hora entero
     * @return horario
     */
    static public int getHorario(int hora)
    {
        if (hora>=0 && hora<6)
        {
            return 1;
        }
        else if(hora>=6 && hora<12)
        {
            return 2;
        }
        else if(hora >=12 && hora<18)
        {
            return 3;
        }
        else if(hora >=18 && hora<24)
        {
            return 4;
        }
                
        return 0;
    }
   /**
    * 1 = Domingo
    * 2 = Lunes
    * 3 = Martes
    * @param dia numero del dia
    * @return nombre del dia segun el numero
    */ 
    static public String getDia(int dia)
    {
        switch(dia)
        {
            case 1:
                return "Domingo";       
               
            case 2:
                 return "Lunes";
                
            case 3:
                return "Martes";
                
            case 4:
                return "Miercoles";
                
            case 5:
                return "Jueves";
                
            case 6:
                return "Viernes";
                       
        }
        return "Sabado"; 

    }
    /**
     * Actualiza las probabilidades de la variable dado un dia, horario,
     * la frecuencia de activacion y la frecuenciencia de no activacion
     * (estos ultimos, sacados de la base de datos)
     * @param dia dia actual en numero 1..7, 0 = Domingo , 1= Lunes,...,6=Sabado
     * @param horario horario en el cual se encontro la evidencia 1..4
     * @param variable nombre del nodo probabilistico, es un sensor
     * @param p0 porcentaje de unos(1) encontrados en la bd
     * @param p1 porcentaje de ceros(0) encontrados en la bd
     * @param net la red probabilistica para poder identificar el nodo que se 
     * actualizará
     * @param file Archivo .net de la red
     * 
     * @return true si se pudo guardar, false en otro caso
     * @throws java.io.FileNotFoundException
     */
    static public boolean actualizarTablaSensor(int dia,
                                           int horario,
                                           String variable,
                                           float p0,
                                           float p1,
                                           ProbabilisticNetwork net,
                                           File file) throws FileNotFoundException
    {
        ProbabilisticNode node = (ProbabilisticNode) net.getNode(variable);
        PotentialTable auxCPT1 = node.getProbabilityFunction();
        
        int[] coordActivo= {1 ,horario-1,dia-1};
        int[] coordNoActivo = {0 ,horario-1,dia-1};
        
        auxCPT1.setValue(coordActivo, p1);
        auxCPT1.setValue(coordNoActivo, p0);
       
       
        
        
        NetIO nIO = new NetIO();
        nIO.save(file ,net);
        
       return false;
    }
    /**
     * 
     * @param dia 1..7
     * @param horario 1..4
     * @param sm 1-0
     * @param sv 1-0
     * @param sp 1-0
     * @param probAlarma probabilidad alarma activa
     * @param net
     * @param file
     * @return
     * @throws FileNotFoundException 
     */
    static public boolean actualizarTablaAlarma(int dia,
                                           int horario,
                                           int sm,
                                           int sv,
                                           int sp,
                                           float probAlarma,                                         
                                           ProbabilisticNetwork net,
                                           File file) throws FileNotFoundException
    {
        ProbabilisticNode node = (ProbabilisticNode) net.getNode(Nodo.alarma.name());
        PotentialTable auxCPT1 = node.getProbabilityFunction();
        sm = (1-sm);
        sv = (1-sv);
        sp = (1-sp);
        int[] coordActivo= {0 ,sm,sp,sv,horario-1,dia-1};
        int[] coordNoActivo = {1 ,sm,sp,sv,horario-1,dia-1};
        
        auxCPT1.setValue(coordActivo, probAlarma);
        auxCPT1.setValue(coordNoActivo, (float)(1-probAlarma));
  
        NetIO nIO = new NetIO();
        nIO.save(file ,net);
       return false;
    }
    
    /**
     * Dada la frecuencia (cantidad de ceros o unos) y el total de registros
     * retorna un float con el porcentaje
     * @param frecuencia cantidad de evidencias de un tipo
     * @param total total de evidencias
     * @return  porcentaje
     */
    static public float getPorcentaje(int frecuencia, int total)
    {
        return (float)((float)frecuencia/(float)total);
    }
    private static float getPorcentaje(float sma, float smt) {
       return (float)(sma/smt);
    }

    /**
     * se debe tener obligatoriamente un archivo .xsl con el nombre del dia
     * por ejemplo Domingo.xsl si el dia es 1.
     * ademas este archivo debe tener 8642 filas lo que significa un dia en intervalos
     * de 10 minutos.
     * 
     * @param dia 1..7 1 = Domingo, 2 = Lunes, ..., 7 = Sabado
     * @throws IOException
     * @throws BiffException
     * @throws WriteException 
     */
    static public void entrenamiento(int dia) throws IOException, BiffException, WriteException
    {
        File conocimiento = new File("imports/conocimiento.xls");
        File conocimientoTmp = new File("imports/ctmp.xls");
        File alarmaFile = new File("imports/alarma.xls");
        File alarmaTmp = new File("imports/atmp.xls");
        Workbook libroExcel = Workbook.getWorkbook(new File("imports/"+getDia(dia)+".xls"));
       
        Workbook book = Workbook.getWorkbook(conocimiento);
        WritableWorkbook wb = Workbook.createWorkbook(conocimientoTmp, book);
        WritableSheet conocimientoSheet = wb.getSheet(0);
        
        Workbook bookAlarma = Workbook.getWorkbook(alarmaFile);
        WritableWorkbook wbalarma = Workbook.createWorkbook(alarmaTmp, bookAlarma);
        WritableSheet alarmaSheet = wbalarma.getSheet(0);

        //Se Carga el archivo con la estructura  
        File file = new File("imports/bayes2.net");       
        NetIO nIO = new NetIO();
        ProbabilisticNetwork net = (ProbabilisticNetwork) nIO.load(file);

        //Se aplica el algoritmo JunctionTree
        JunctionTreeAlgorithm alg = new JunctionTreeAlgorithm();
        alg.setNetwork(net);
        alg.run();
        /**
         * **** Ahora empieza el Algoritmo ****
         */
        
        
        
        //Sheet sheet = libroExcel.getSheet(diaActual-1);
        Sheet sheet = libroExcel.getSheet(0);
       
            int filaExcel = 2;
            int diaActual = dia-1, horario=1, aux, alarmaAntigua=0,alarmaAntiguat=0;
            float sma=0,smt=0, sva=0,svt=0, spa=0,spt=0, sa=0,sat=0, aAux=0, alarmat=0;
            int sm=0,sv=0,sp=0;
        while(filaExcel < 8642)
        {
 
             //horario hora del dia
             aux = Integer.parseInt(sheet.getCell(0, filaExcel).getContents().split(":")[0]);
            
             if(horario != getHorario(aux))
             {
            
                 try {
                     //agregar a la base de conocimiento
                     conocimientoSheet.addCell(new jxl.write.Label(0, (diaActual*4)+(horario), getDia(dia)));
                     conocimientoSheet.addCell(new jxl.write.Number(1, (diaActual*4)+(horario), horario));
                     conocimientoSheet.addCell(new jxl.write.Number(2, (diaActual*4)+(horario), sma));
                     conocimientoSheet.addCell(new jxl.write.Number(3, (diaActual*4)+(horario), smt));
                     conocimientoSheet.addCell(new jxl.write.Number(4, (diaActual*4)+(horario), sva));
                     conocimientoSheet.addCell(new jxl.write.Number(5, (diaActual*4)+(horario), svt));
                     conocimientoSheet.addCell(new jxl.write.Number(6, (diaActual*4)+(horario), spa));
                     conocimientoSheet.addCell(new jxl.write.Number(7, (diaActual*4)+(horario), spt));
                     
                     

                     //actualizo las tablas de probabilidad  SM
                     actualizarTablaSensor(diaActual+1, horario,Nodo.SM.name(), getPorcentaje(sma,smt), 1- getPorcentaje(sma,smt), net, file);
         
                     //actualizo las tablas de probabilidad  SV
                     actualizarTablaSensor(diaActual+1, horario,Nodo.SV.name(), getPorcentaje(sva,svt), 1- getPorcentaje(sva,svt), net, file);
                     
                    //actualizo las tablas de probabilidad  SP
                     actualizarTablaSensor(diaActual+1, horario,Nodo.SP.name(), getPorcentaje(sma,spt), 1- getPorcentaje(sma,spt), net, file);
                     
                    //actualizo las tablas de probabilidad de la alarma
                     
                     
                     nIO.save(file ,net);
                     
                     sma=0;smt=0; sva=0;svt=0; spa=0;spt=0;
                 } catch (WriteException ex) {
                     Logger.getLogger(BayesNetwork.class.getName()).log(Level.SEVERE, null, ex);
                 }
             }
             
             horario = getHorario(aux);
             
             //sensonr de movimiento
             if(Integer.parseInt( sheet.getCell(1, filaExcel).getContents() ) ==1)
             {
                 
                 sma++;sm=1;
             }
             smt++;
             //sensor ventana
             if(Integer.parseInt( sheet.getCell(2, filaExcel).getContents() ) ==1)
             {
                 sva++;sv=1;
             }
             svt++;
             //sensor puerta
             if(Integer.parseInt( sheet.getCell(3, filaExcel).getContents() )==1)
             {
                 spa++;sp=1;
             }
             spt++;
             
             //alarma
             alarmaAntigua = Integer.parseInt( alarmaSheet.getCell(5, (diaActual*32)+((horario-1)*8)+(sm*4+sv*2+sp+1)).getContents() );
             alarmaAntiguat = Integer.parseInt( alarmaSheet.getCell(6, (diaActual*32)+((horario-1)*8)+(sm*4+sv*2+sp+1)).getContents() );
             sa = Integer.parseInt( sheet.getCell(4, filaExcel).getContents() );
             aAux =  (alarmaAntigua + sa);
             alarmat = alarmaAntiguat+ 1;
             alarmaSheet.addCell(new jxl.write.Label(0, (diaActual*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), getDia(dia)));
             alarmaSheet.addCell(new jxl.write.Number(1,(diaActual*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), horario));
             alarmaSheet.addCell(new jxl.write.Number(2, (diaActual*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), sm));
             alarmaSheet.addCell(new jxl.write.Number(3, (diaActual*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), sv));
             alarmaSheet.addCell(new jxl.write.Number(4, (diaActual*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), sp));
             alarmaSheet.addCell(new jxl.write.Number(5, (diaActual*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), aAux)); //alarma
             alarmaSheet.addCell(new jxl.write.Number(6, (diaActual*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), alarmat)); // alarma total
             actualizarTablaAlarma(diaActual+1, horario, sm, sv, sp, getPorcentaje(aAux, alarmat), net, file);
             
             filaExcel++;
             sm=0;
             sv=0;
             sp=0;
             
        } 
        
           
        wbalarma.write();
        wbalarma.close();
        wb.write();
        wb.close();
        bookAlarma.close();
        book.close();
        conocimiento.delete();
        conocimientoTmp.renameTo(new File("imports/conocimiento.xls"));
        alarmaFile.delete();
        alarmaTmp.renameTo(new File("imports/alarma.xls"));
    }
    /**
     * Se actualiza la base de conocimiento la cual consta de un archivo excel
     * con un formato de :
     * dia|horario|sm|smt|sv|svt|sp|spt|a|at
     * y del archivo .net en el cual esta la red bayesiana y las tablas probabilisticas
     * 
     * @param net red probabilistica
     * @param nIO input output de la red
     * @param file archivo de la red
     * @param dia dia que se tomo la evidencia 1...7
     * @param horario horario 1..4
     * @param sm sensor de movimiento 1-0
     * @param sv sensor de ventana 1-0
     * @param sp sensor de puerta 1-0
     * @param alarma alarma 1-0
     * @throws IOException
     * @throws BiffException 
     */
    static public void actualizarConocimiento( ProbabilisticNetwork net, NetIO nIO, File file, int dia, int horario,int sm,int sv,int sp, int alarma ) throws IOException, BiffException
    {
                File conocimiento = new File("imports/conocimiento.xls");
                File conocimientoTmp = new File("imports/ctmp.xls");
                File alarmaFile = new File("imports/alarma.xls");
                File alarmaTmp = new File("imports/atmp.xls");
                dia=dia-1;
                int smt, svt, spt, alarmat, smAux, svAux, spAux, aAux;
                Workbook book = Workbook.getWorkbook(conocimiento);
                WritableWorkbook wb = Workbook.createWorkbook(conocimientoTmp, book);
                WritableSheet conocimientoSheet = wb.getSheet(0);
                
                Workbook bookAlarma = Workbook.getWorkbook(alarmaFile);
                //Sheet alarmaActSheet = bookAlarma.getSheet(0);
                WritableWorkbook wbAlarma = Workbook.createWorkbook(alarmaTmp, bookAlarma);
                WritableSheet alarmaSheet = wbAlarma.getSheet(0);
                 try {
                     //leer la base
                     if(sm==1)
                     {  
                         smAux = Integer.parseInt( conocimientoSheet.getCell(2, (dia*4)+(horario)).getContents() )  + 1;
                     }
                     else
                     {
                         smAux = Integer.parseInt( conocimientoSheet.getCell(2, (dia*4)+(horario)).getContents() );
                     }
                     smt = Integer.parseInt( conocimientoSheet.getCell(3, (dia*4)+(horario)).getContents() ) +1;
                     
                     if(sv==1)
                     {  
                         svAux = Integer.parseInt( conocimientoSheet.getCell(4, (dia*4)+(horario)).getContents() )  + 1;
                     }
                     else
                     {
                         svAux = Integer.parseInt( conocimientoSheet.getCell(4, (dia*4)+(horario)).getContents() );
                     }
                     svt = Integer.parseInt( conocimientoSheet.getCell(5, (dia*4)+(horario)).getContents() ) +1;
                     
                      if(sp==1)
                     {  
                         spAux = Integer.parseInt( conocimientoSheet.getCell(6, (dia*4)+(horario)).getContents() )  + 1;
                     }
                     else
                     {
                         spAux = Integer.parseInt( conocimientoSheet.getCell(6, (dia*4)+(horario)).getContents() );
                     }
                     spt = Integer.parseInt( conocimientoSheet.getCell(7, (dia*4)+(horario)).getContents() ) +1;
                     
                     if(alarma==1)
                     {  
                         aAux = Integer.parseInt( alarmaSheet.getCell(5, ((horario-1)*8)+(sm*4+sv*2+sp+1)).getContents() )  + 1;
                     }
                     else
                     {
                         aAux = Integer.parseInt( alarmaSheet.getCell(5, ((horario-1)*8)+(sm*4+sv*2+sp+1)).getContents() );
                     }
                     alarmat = Integer.parseInt( alarmaSheet.getCell(6, ((horario-1)*8)+(sm*4+sv*2+sp+1)).getContents() ) +1;
                     
                     
                     //agregar a la base de conocimiento
                     conocimientoSheet.addCell(new jxl.write.Label(0, (dia*4)+(horario), getDia(dia+1)));
                     conocimientoSheet.addCell(new jxl.write.Number(1, (dia*4)+(horario), horario));
                     conocimientoSheet.addCell(new jxl.write.Number(2, (dia*4)+(horario), smAux));
                     conocimientoSheet.addCell(new jxl.write.Number(3, (dia*4)+(horario), smt));
                     conocimientoSheet.addCell(new jxl.write.Number(4, (dia*4)+(horario), svAux));
                     conocimientoSheet.addCell(new jxl.write.Number(5, (dia*4)+(horario), svt));
                     conocimientoSheet.addCell(new jxl.write.Number(6, (dia*4)+(horario), spAux));
                     conocimientoSheet.addCell(new jxl.write.Number(7, (dia*4)+(horario), spt));
                     
                     
                     actualizarAlarmaSheet(dia, horario, sm, sv, sp, aAux, alarmat, alarmaSheet);
                     
                     

                     //actualizo las tablas de probabilidad  SM
                     actualizarTablaSensor(dia+1, horario,Nodo.SM.name(), getPorcentaje(smAux,smt), 1- getPorcentaje(smAux,smt), net, file);
                     
                     //actualizo las tablas de probabilidad  SV
                     actualizarTablaSensor(dia+1, horario,Nodo.SV.name(), getPorcentaje(svAux,svt), 1- getPorcentaje(svAux,svt), net, file);
                    //actualizo las tablas de probabilidad  SP
                     actualizarTablaSensor(dia+1, horario,Nodo.SP.name(), getPorcentaje(spAux,spt), 1- getPorcentaje(spAux,spt), net, file);
                     
                    //actualizo las tablas de probabilidad de la alarma
                     int sma=sm, sva=sv, spa=sp;
                    if((sm==1|| sm==0)&&(sv==1 || sv==0)&&(sp==1 || sp==0))
                    {
                         actualizarTablaAlarma(dia+1, horario, sm, sv, sp, getPorcentaje(aAux,alarmat), net, file);
                    }
                    else
                    {
                        for (int j = 0; j < 8; j++) 
                        {
                            if(sma!=0 && sma!=1)
                            {
                             if(j<4)  {sm=1;}
                             else     {sm=0;}
                            }
                            if(sva!=0 && sva!=1)
                            {
                                if(j<4)  {sv=1;}
                                else     {sv=0;}
                            }
                            if(spa!=0 && spa!=1 )
                            {
                                if(j<4)  {sp=1;}
                                else     {sp=0;}
                            }
                            actualizarTablaAlarma(dia+1, horario, sm, sv, sp, getPorcentaje(aAux,alarmat), net, file);
                        }
                    }
                    
                    nIO.save(file ,net);
                    wbAlarma.write();
                    wbAlarma.close();
                    wb.write();
                    wb.close();
                    bookAlarma.close();
                    book.close();
                    
                    conocimiento.delete();
                    conocimientoTmp.renameTo(new File("imports/conocimiento.xls"));
                    alarmaFile.delete();
                    alarmaTmp.renameTo(new File("imports/alarma.xls"));
                    
                     
                     
                 } catch (WriteException ex) {
                     Logger.getLogger(BayesNetwork.class.getName()).log(Level.SEVERE, null, ex);
                 }
    }
    static private void actualizarAlarmaSheet(int dia, int horario, int sm, int sv, int sp, int aAux, int alarmat, WritableSheet alarmaSheet) throws WriteException
    {
        if((sm==1|| sm==0)&&(sv==1 || sv==0)&&(sp==1 || sp==0))
        {
            alarmaSheet.addCell(new jxl.write.Label(0, (dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), getDia(dia+1)));
            alarmaSheet.addCell(new jxl.write.Number(1,(dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), horario));
            alarmaSheet.addCell(new jxl.write.Number(2, (dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), sm));
            alarmaSheet.addCell(new jxl.write.Number(3, (dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), sv));
            alarmaSheet.addCell(new jxl.write.Number(4, (dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), sp));
            alarmaSheet.addCell(new jxl.write.Number(5, (dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), aAux)); //alarma
            alarmaSheet.addCell(new jxl.write.Number(6, (dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), alarmat)); // alarma total
        }
        else
        {
            int sma=sm, sva=sv, spa=sp;
            for (int j = 0; j < 8; j++) 
            {
                if(sma!=0 && sma!=1)
                {
                 if(j<4)  {sm=1;}
                 else     {sm=0;}
                }
                if(sva!=0 && sva!=1)
                {
                    if(j<4)  {sv=1;}
                    else     {sv=0;}
                }
                if(spa!=0 && spa!=1 )
                {
                    if(j<4)  {sp=1;}
                    else     {sp=0;}
                }
                alarmaSheet.addCell(new jxl.write.Label(0, (dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), getDia(dia+1)));
                alarmaSheet.addCell(new jxl.write.Number(1,(dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), horario));
                alarmaSheet.addCell(new jxl.write.Number(2, (dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), sm));
                alarmaSheet.addCell(new jxl.write.Number(3, (dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), sv));
                alarmaSheet.addCell(new jxl.write.Number(4, (dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), sp));
                alarmaSheet.addCell(new jxl.write.Number(5, (dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), aAux)); //alarma
                alarmaSheet.addCell(new jxl.write.Number(6, (dia*32) +((horario-1)*8)+(sm*4+sv*2+sp+1), alarmat)); // alarma total
            }
        }  
        
        
    }
}

