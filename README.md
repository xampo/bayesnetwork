# README #

This README document  steps  necessary to get this application up and running.
additionaly gives us some snippet of code to use Unbbayes API

### What is this repository for? ###

* This is a Java project using Unbbayes API to solve a security problem considering three sensors(moving, door, window), an alarm, and taking two variables for clasification: hour of day(in ranges of 6) and day of week. 

* Just for learning IA and probabilistics networks


* Version 0.0.01


### How do I get set up? ###

* This code is developed on NetBeans 8.0 so whoever  can download it in https://netbeans.org/downloads/, and then open it.
* also, we have to download unbbayes api from http://unbbayes.sourceforge.net and import as library.


### Contribution guidelines ###
here we have some example of code:

**Load a file**
        
```
#!java

File file = new File("bayes.net");       
        NetIO nIO = new NetIO();
        ProbabilisticNetwork net = (ProbabilisticNetwork) nIO.load(file);

```

**JunctionTree Algorithm**

```
#!java

        JunctionTreeAlgorithm alg = new JunctionTreeAlgorithm();
        alg.setNetwork(net);
        alg.run();
```


**Insert Evidence**

```
#!java

        ProbabilisticNode findingNode = (ProbabilisticNode)net.getNode(Nodo.SM.name());
        findingNode.addFinding(0); // el primer estado ahora es 100% (activo)
        net.updateEvidences();
```


**Set and Save probabilistic tables on File.net**



```
#!java

      ProbabilisticNode node1 = (ProbabilisticNode) net.getNode("name");
      PotentialTable auxCPT1 = node1.getProbabilityFunction();
      auxCPT1.setValue(0, (float) 0.5);
      auxCPT1.setValue(1, (float)0.5);
      nIO.save(file ,net);
```
**Get and Print probabilities for each Node**


```
#!java

for (Node node2 : net.getNodes()) 
        { 
            System.out.println(node2.getName()+"" + node2.getDescription());
            for (int i = 0; i < node2.getStatesSize(); i++) 
            {
                
                System.out.println(""+ node2.getStateAt(i) + " : " + ((ProbabilisticNode)node2).getMarginalAt(i));
            }
        }

```


      
# LEAME #

Este  README documenta los pasos necesarios para poner esta aplicación en funcionamiento.
adicionalmente nos da algunos fragmento de código para utilizar UnBBayes API

### ¿Para qué es este repositorio? ###

* Se trata de un proyecto Java utilizando la API UnBBayes para resolver un problema de seguridad teniendo en cuenta tres sensores (de movimiento, puertas, ventanas), una alarma, y tomando dos variables para clasificación: hora del día (en rangos de 6) y el día de la semana. 

*Sólo para el aprendizaje de IA y probabilística redes


* Versión 0.0.01


### ¿Cómo lo puedo configurar? ###

* Este código es desarrollado en NetBeans 8.0 así que quien quiera puede descargarlo en https://netbeans.org/downloads/, y luego abrirlo.
* También, tenemos que descargar api UnBBayes de http://unbbayes.sourceforge.net e importarla como biblioteca.


### Directrices Contribución ###
Aquí tenemos algunos ejemplos de código:

**Cargar el archivo**

```
#!java

        File file = new File("bayes.net");       
        NetIO nIO = new NetIO();
        ProbabilisticNetwork net = (ProbabilisticNetwork) nIO.load(file);
```


**Algoritmo JunctionTree**

  
```
#!java

      JunctionTreeAlgorithm alg = new JunctionTreeAlgorithm();
        alg.setNetwork(net);
        alg.run();
```


**Insertar evidencia**

  
```
#!java

        ProbabilisticNode findingNode = (ProbabilisticNode)net.getNode(Nodo.SM.name());
        findingNode.addFinding(0); // el primer estado ahora es 100% (activo)
        net.updateEvidences();
```


**Cambiar y guardar las probabilidades en el archivo.net**

   
```
#!java

   ProbabilisticNode node1 = (ProbabilisticNode) net.getNode("name");
      PotentialTable auxCPT1 = node1.getProbabilityFunction();
      auxCPT1.setValue(0, (float) 0.5);
      auxCPT1.setValue(1, (float)0.5);
      nIO.save(file ,net);
```

**Obtener e imprimir probabilidades de cada Nodo**


```
#!java

for (Node node2 : net.getNodes()) 
        { 
            System.out.println(node2.getName()+"" + node2.getDescription());
            for (int i = 0; i < node2.getStatesSize(); i++) 
            {
                
                System.out.println(""+ node2.getStateAt(i) + " : " + ((ProbabilisticNode)node2).getMarginalAt(i));
            }
        }

```